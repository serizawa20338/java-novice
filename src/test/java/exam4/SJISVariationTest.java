package exam4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SJISVariationTest {
    SJISVariation sut;

    @BeforeEach
    void setup() {
        sut = null;
    }

    @Test
    void containsUnMatchChars() {
        assertThat(sut.findUnMatchChars()).containsExactlyInAnyOrder('―', '～', '∥', '－', '￤');
    }

}