package exam6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BranchNameValidatorTest {
    BranchNameValidator sut;

    @BeforeEach
    void setup() {
        sut = null;
    }

    @Test
    void valid() {
        assertThat(sut.validate("a/b")).isTrue();
    }

    @Test
    void startsWithDotIsInvalid() {
        assertThat(sut.validate("a/.b")).isFalse();
    }

    @Test
    void containsDotDotIsInvalid() {
        assertThat(sut.validate("a..b")).isFalse();
    }

    @Test
    void containsSpaceIsInvalid() {
        assertThat(sut.validate("a b")).isFalse();
    }

    @Test
    void endsWithSlashIsInvalid() {
        assertThat(sut.validate("a/b/")).isFalse();
    }

    @Test
    void endsWithDotLockIsInvalid() {
        assertThat(sut.validate("a/b.lock")).isFalse();
    }

}