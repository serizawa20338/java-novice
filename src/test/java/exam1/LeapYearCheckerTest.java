package exam1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class LeapYearCheckerTest {
    LeapYearChecker sut;

    @BeforeEach
    void setup() {
        sut = null;
    }

    @Test
    public void year1999IsNotLeap() {
        assertThat(sut.isLeapYear(1999)).isFalse();
    }
    @Test
    public void year2000IsLeap() {
        assertThat(sut.isLeapYear(2000)).isTrue();
    }
    @Test
    public void year2004IsLeap() {
        assertThat(sut.isLeapYear(2004)).isTrue();
    }
    @Test
    public void year2100IsNotLeap() {
        assertThat(sut.isLeapYear(2100)).isFalse();
    }

}
