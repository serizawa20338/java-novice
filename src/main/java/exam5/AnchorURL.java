package exam5;

import java.util.regex.Pattern;

public interface AnchorURL {
    Pattern URL_PTN = Pattern.compile("(http(s)?://.)?(www\\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_+.~#?&/=]*)",
            Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
    Pattern TAG_NAME_PTN = Pattern.compile("[^\"'<>]*(?:\"[^\"]*\"[^\"'<>]*|'[^']*'[^\"'<>]*)*(?:>|(?=<)|$(?!\\n))");
    Pattern COMMENT_TAG_PTN = Pattern.compile("<!(?:--[^-]*-(?:[^-]+-)*?-(?:[^>-]*(?:-[^>-]+)*?)??)*(?:>|$(?!\\n)|--.*$)");
    Pattern TAG_PTN = Pattern.compile(COMMENT_TAG_PTN.pattern() + "|<"+ TAG_NAME_PTN.pattern());

    String convert(String body);
}
