package exam2;

public interface CardNumberValidator {
    boolean isValid(String cardNumber);
}
