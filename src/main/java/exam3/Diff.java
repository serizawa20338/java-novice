package exam3;

import java.util.HashSet;
import java.util.Set;

public class Diff {
    private final Set<User> addedUsers = new HashSet<>();
    private final Set<User> modifiedUsers = new HashSet<>();
    private final Set<User> removedUsers = new HashSet<>();

    public void add(User user) {
        addedUsers.add(user);
    }

    public void modify(User user) {
        modifiedUsers.add(user);
    }

    public void remove(User user) {
        removedUsers.add(user);
    }

    public Set<User> getAddedUsers() {
        return addedUsers;
    }

    public Set<User> getModifiedUsers() {
        return modifiedUsers;
    }

    public Set<User> getRemovedUsers() {
        return removedUsers;
    }

    @Override
    public String toString() {
        return "Diff{" +
                "addedUsers=" + addedUsers +
                ", modifiedUsers=" + modifiedUsers +
                ", removedUsers=" + removedUsers +
                '}';
    }
}
