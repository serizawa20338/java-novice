package exam1;

public interface LeapYearChecker {
    boolean isLeapYear(int year);
}
